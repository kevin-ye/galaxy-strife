﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GSMainMenuGUIController : MonoBehaviour {

    public InputField idInput;
    public Button loginButton;


    public void Start()
    {
        GSNetwork.network_handler.Instance.registerLoginCallback(delegate(GSNetwork.GSLoginPacket packet) { LoginCallback(packet); });
    }

    private void LoginCallback(GSNetwork.GSPacket packet)
    {
    }

    public void Login()
    {
        GSNetwork.network_handler.Instance.login(idInput.text);
        //GSNetwork.network_handler.Instan   ce.login();
    }

}
