﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GSGroundHex : GSHex
{
    public override void MarkAsReachable()
    {
        SetColor(Color.yellow);
    }
    public override void MarkAsPath()
    {
        SetColor(Color.green); ;
    }
    public override void MarkAsHighlighted()
    {
        SetOutlineColor(Color.blue);
    }

    public override void MarkAsEnemy()
    {
        SetColor(Color.red);
    }

    public override void MarkAsSelected()
    {
        SetOutlineColor(Color.green);
    }

    public override void UnMark()
    {
        SetColor(Color.white);
        SetOutlineColor(Color.black);
    }
}