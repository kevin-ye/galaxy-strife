using UnityEngine;
using System;
using System.Collections.Generic;

public abstract class GSHex : Hexagon
{
    public GSUnit unit;

    public GameObject defaultChild;
    public GameObject outlineChild;
    
    public Collider collider;

    public int height;

    public void Start()
    {
        UnMark();

    }

    public void ChildMouseDown()
    {
        OnMouseDown();
    }

    public override Vector3 GetCellDimensions()
	{
		var center = Vector3.zero;
		center += transform.GetChild(0).GetComponent<Renderer>().bounds.center;
		
		Bounds ret = new Bounds (center, Vector3.zero);
		
		ret.Encapsulate(transform.GetChild(0).GetComponent<Renderer>().bounds);
		ret.Expand (0.2f);
		return ret.size;
	}
	
    public void MakeSelectable()
    {
        collider.enabled = true;
        var components = GetComponentsInChildren<Renderer>();
        foreach (Renderer child in components)
        {
            //Vector4 newColor = child.material.color;
            //newColor.w = 1;
            //child.material.color = newColor;
            child.enabled = true;
        }
        if (unit)
        {
            unit.MakeSelectable();
        }
    }

    public void MakeUnselectable()
    {
        collider.enabled = false;
        Renderer[] components = GetComponentsInChildren<Renderer>();
        foreach (Renderer child in components)
        {
            //Vector4 newColor = child.material.color;
            //newColor.w = GSCommon.unselectableAlpha;
            //child.material.color = newColor;
            child.enabled = false;
        }
        if (unit)
        {
            unit.MakeUnselectable();
        }
    }

    public abstract override void MarkAsReachable();

    public abstract override void MarkAsPath();

    public abstract override void MarkAsHighlighted();

    public abstract void MarkAsEnemy();

    public abstract void MarkAsSelected();

    public abstract override void UnMark();
    	
	protected void SetColor(Color color)
	{
		Transform defaultchild = transform.FindChild("default");
		Renderer rendererComponent = defaultchild.transform.GetComponent<Renderer>();
        if (rendererComponent != null) {
            Vector4 newColor = color;
            newColor.w = rendererComponent.material.color.a;
            rendererComponent.material.color = newColor;
        }

	}
	protected void SetOutlineColor(Color color)
	{
		Transform outline = transform.FindChild("outline");
		Renderer rendererComponent = outline.transform.GetComponent<Renderer>();
        if (rendererComponent != null)
        {
            Vector4 newColor = color;
            newColor.w = rendererComponent.material.color.a;
            rendererComponent.material.color = newColor;
        }
    }

	public void SetUnit(GSUnit unit){
        this.unit = unit;
        IsTaken = true;
        
	}

	public void RemoveUnit(){
        this.unit = null;
        IsTaken = false;
    }

    public override int GetDistance(Cell other)
    {
        GSHex otherHex = (GSHex) other;
        //Debug.Log(CubeCoord);
        //Debug.Log(otherHex.CubeCoord);
        int distance = (int)((Mathf.Abs(CubeCoord.x - otherHex.CubeCoord.x) + Mathf.Abs(CubeCoord.y - otherHex.CubeCoord.y) + Mathf.Abs(CubeCoord.z - otherHex.CubeCoord.z)) / 2 + Mathf.Abs(height - otherHex.height));
        //if (otherHex.height == 2)
        //    Debug.Log(name + " to " + other.name + " is " + distance);
        return distance;

        //GSHex otherHex = (GSHex)other;
        //int distance = (int)(Mathf.Sqrt(
        //   Mathf.Pow((Mathf.Abs(CubeCoord.x - otherHex.CubeCoord.x) +
        //    Mathf.Abs(CubeCoord.y - otherHex.CubeCoord.y) +
        //    Mathf.Abs(CubeCoord.z - otherHex.CubeCoord.z)) / 2, 2) +
        //    Mathf.Pow(Mathf.Abs(height - otherHex.height), 2)));
        //if (otherHex.height == 2)
        //    Debug.Log(name + " to " + other.name + " is " + distance);
        //return distance;
    
}

    public override void InitNeighbours(List<Cell> cells)
    {

        AllNeighbours = new List<Cell>();
        Cell neighbour;
        foreach (var direction in _directions)
        {
            neighbour = cells.Find(c => c.OffsetCoord == CubeToOffsetCoords(CubeCoord + direction) && height == ((GSHex)c).height);
            if (neighbour == null) continue;
            AllNeighbours.Add(neighbour);
        }
        List<Cell> neighbours = cells.FindAll(c => (c.OffsetCoord == OffsetCoord && ((GSHex)c).height == height + 1) || (c.OffsetCoord == OffsetCoord && ((GSHex)c).height == height - 1));
        foreach(Cell vertNeighbour in neighbours)
        {
            AllNeighbours.Add(vertNeighbour);
        }

    }
}

