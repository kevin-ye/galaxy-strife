public class GSGameStateAiTurn : GSGameState
{
	public GSGameStateAiTurn(GSBattleSceneManager battleSceneManager) : base(battleSceneManager)
	{      
	}
	
	public override void OnStateEnter()
	{
		base.OnStateEnter();
		foreach (var cell in _battleSceneManager.CellGrid.Cells)
		{
			cell.UnMark();
		}
	}
}