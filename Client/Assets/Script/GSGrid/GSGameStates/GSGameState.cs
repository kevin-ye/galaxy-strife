using System.Linq;
using UnityEngine;

public abstract class GSGameState
{
	protected GSBattleSceneManager _battleSceneManager;
	
	protected GSGameState(GSBattleSceneManager battleSceneManager)
	{
        _battleSceneManager = battleSceneManager;
	}
	
	public virtual void OnUnitClicked(Unit unit)
	{ 
	}
	
	public virtual void OnCellDeselected(Cell cell)
	{
		cell.UnMark();
	}
	public virtual void OnCellSelected(Cell cell)
	{
		cell.MarkAsHighlighted();
	}
	public virtual void OnCellClicked(Cell cell)
	{
		//cell.MarkAsHighlighted();
	}
	
	public virtual void OnStateEnter()
	{
        if (_battleSceneManager.UnitManager.units.Select(u => u.PlayerNumber).Distinct().ToList().Count == 1)
		{
            _battleSceneManager.GameState = new GSGameStateGameOver(_battleSceneManager);
		}
	}
	public virtual void OnStateExit()
	{
	}
}