using System.Linq;
using UnityEngine;

class GSGameStateWaitingForInput : GSGameState
{
	public GSGameStateWaitingForInput(GSBattleSceneManager battleSceneManager) : base(battleSceneManager)
    {
	}

    public override void OnCellClicked(Cell cell)
    {
        //foreach(Cell neighbour in cell.GetNeighbours())
        //{
        //    neighbour.MarkAsReachable();
        //}
        base.OnCellClicked(cell);
    }

    public override void OnUnitClicked(Unit unit)
	{
        //Debug.Log(unit);
		if(unit.PlayerNumber.Equals(_battleSceneManager.CurrentPlayerNumber))
			_battleSceneManager.GameState = new GSGameStateUnitSelected(_battleSceneManager, unit as GSUnit); 
	}

	//public override void OnCellClicked(Cell cell)
	//{
 //       _cellGrid.CellGridState = new GSCellGridStateCellSelected(_cellGrid, cell);
        
	//}
}
