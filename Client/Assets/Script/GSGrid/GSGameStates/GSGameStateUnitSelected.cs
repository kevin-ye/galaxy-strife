using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class GSGameStateUnitSelected : GSGameState
{
	private GSUnit _unit;
	private List<Cell> _pathsInRange;
    private List<Cell> _enemyCellsInRange;
    private List<Unit> _unitsInRange;
	
	private Cell _unitCell;
	
	public GSGameStateUnitSelected(GSBattleSceneManager battleSceneManager, GSUnit unit) : base(battleSceneManager)
	{
		_unit = unit;
		_pathsInRange = new List<Cell>();
        _enemyCellsInRange = new List<Cell>();
        _unitsInRange = new List<Unit>();

    }
	
	public override void OnCellClicked(Cell cell)
	{
        if (_unit.isMoving)
			return;
        if (_enemyCellsInRange.Contains(cell))
        {
            foreach (Unit unit in _unitsInRange)
            {
                if (unit.Cell == cell)
                {
                    OnUnitClicked(unit);
                    return;
                }
                        
            }
            
        }
		if(cell.IsTaken)
		{
			_battleSceneManager.GameState= new GSGameStateWaitingForInput(_battleSceneManager);
			return;
		}
		
		if(!_pathsInRange.Contains(cell))
		{
            _battleSceneManager.GameState = new GSGameStateWaitingForInput(_battleSceneManager);
        }
		else
		{
            GSUnit gsunit = (GSUnit)_unit;
            GSHex gshex = (GSHex)cell;
            
            var path = _unit.FindPath(_battleSceneManager.CellGrid.Cells, cell);
			GSBattleSceneManager.Instance.AddToUndoStack(new GSActionMove(gsunit, (GSHex)_unit.Cell, gshex, path.Sum(h => h.MovementCost)));

            _unit.Move(cell,path);
            
            
			_battleSceneManager.GameState= new GSGameStateUnitSelected(_battleSceneManager, _unit);
		}
	}
	public override void OnUnitClicked(Unit unit)
	{
		if (unit.Equals(_unit) || unit.isMoving)
			return;
		
		if (_unitsInRange.Contains(unit) && _unit.ActionPoints > 0)
		{
			GSBattleSceneManager.Instance.AddToUndoStack(new GSActionAttack((GSUnit)_unit, (GSUnit)unit, 1));
            _unit.DealDamage(unit);
            if (_unit.ActionPoints > 0)
			    _battleSceneManager.GameState = new GSGameStateUnitSelected(_battleSceneManager, _unit);
            else
                _battleSceneManager.GameState = new GSGameStateWaitingForInput(_battleSceneManager);
		}
		
		if (unit.PlayerNumber.Equals(_unit.PlayerNumber))
		{
            _battleSceneManager.GameState = new GSGameStateUnitSelected(_battleSceneManager, unit as GSUnit);
		}
		
	}
	public override void OnCellDeselected(Cell cell)
	{
		base.OnCellDeselected(cell);
		
		foreach (var _cell in _pathsInRange)
		{
			_cell.MarkAsReachable();
		}

        foreach (var _cell in _enemyCellsInRange)
        {
            ((GSHex)_cell).MarkAsEnemy();
        }

        foreach (var _cell in _battleSceneManager.CellGrid.Cells.Except(_pathsInRange).Except(_enemyCellsInRange))
		{
            
			_cell.UnMark();
		}
	}
	public override void OnCellSelected(Cell cell)
	{
        
        base.OnCellSelected(cell);
        
		if (!_pathsInRange.Contains(cell)) return;
		var path = _unit.FindPath(_battleSceneManager.CellGrid.Cells, cell);
		foreach (var _cell in path)
		{
			_cell.MarkAsPath();
		}
	}
	
	public override void OnStateEnter()
	{
		base.OnStateEnter();
		
		_unit.OnUnitSelected();
		_unitCell = _unit.Cell;
		
		_pathsInRange = _unit.GetAvailableDestinations(_battleSceneManager.CellGrid.Cells);
		var cellsNotInRange = _battleSceneManager.CellGrid.Cells.Except(_pathsInRange);
		
		foreach (var cell in cellsNotInRange)
		{
			cell.UnMark();
		}
		foreach (var cell in _pathsInRange)
		{
			cell.MarkAsReachable();
		}
		
		if (_unit.ActionPoints <= 0) return;
		
		foreach (var currentUnit in _battleSceneManager.UnitManager.units)
		{
			if (currentUnit.PlayerNumber.Equals(_unit.PlayerNumber))
				continue;
			
			var cell = currentUnit.Cell;
            if (cell.GetDistance(_unitCell) <= _unit.AttackRange)
			{
                
                currentUnit.SetState(new UnitStateMarkedAsReachableEnemy(currentUnit));
                ((GSUnit)currentUnit).MarkAsEnemy();
				_unitsInRange.Add(currentUnit);
                _enemyCellsInRange.Add(currentUnit.Cell);
			}
		}
		
		if (_unitCell.GetNeighbours().FindAll(c => c.MovementCost <= _unit.MovementPoints).Count == 0 
		    && _unitsInRange.Count == 0)
			_unit.SetState(new UnitStateMarkedAsFinished(_unit));
	}
	public override void OnStateExit()
	{
		_unit.OnUnitDeselected();
		foreach (var unit in _unitsInRange)
		{
			if (unit == null) continue;
			unit.SetState(new UnitStateNormal(unit));
		}
		foreach (var cell in _battleSceneManager.CellGrid.Cells)
		{
			cell.UnMark();
		}   
	}
}

