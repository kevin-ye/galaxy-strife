﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GSUnitManager : MonoBehaviour {
    public List<GSUnit> player0Units;
    public List<GSUnit> player1Units;
    public List<Unit> units;


    public event EventHandler UnitClicked;
    public event EventHandler<AttackEventArgs> UnitDestroyed;
    // Use this for initialization
    void Start()
    {
        
    }

    public void StartLevel(){

        var unitGenerator = GSBattleSceneManager.Instance.GetComponent<IUnitGenerator>();
        if (unitGenerator != null)
        {
            units = unitGenerator.SpawnUnits(GSBattleSceneManager.Instance.CellGrid.Cells);
            foreach (var unit in units)
            {
                unit.UnitClicked += OnUnitClicked;
                unit.UnitDestroyed += OnUnitDestroyed;
            }
        }
        else
            Debug.LogError("No IUnitGenerator script attached to cell grid");

        units.FindAll(u => u.PlayerNumber.Equals(GSBattleSceneManager.Instance.CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnUnitClicked(object sender, EventArgs e)
    {
        UnitClicked.Invoke(sender, e);
    }

    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        UnitDestroyed.Invoke(sender, e);
    }
}
