﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GSBattleGUIController : MonoBehaviour {
    public static GSBattleGUIController Instance { get; private set; }

    public GameObject buttonPrefab;
    public GameObject LayerSelectPanel;

    public Button undoButton;
    public Button redoButton;

    public Button startButton;
    public Button nextTurnButton;

    public Text infoText;

    List<Button> layerButtons;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        GSBattleSceneManager.Instance.ActionStackChanged += OnActionStackChanged;
        GSBattleSceneManager.Instance.GameStarted += OnGameStarted;
        GSBattleSceneManager.Instance.GameEnded += OnGameEnded;
        GSBattleSceneManager.Instance.TurnEnded += OnTurnEnded;
        GSBattleSceneManager.Instance.LayerChanged += OnLayerChanged;
        layerButtons = new List<Button>();
	    for(int i = 0; i < GSCommon.layers; i++)
        {
            GameObject newButton = Instantiate(buttonPrefab, new Vector3(0,i * 30 - 50,0),Quaternion.identity) as GameObject;
            newButton.transform.SetParent(LayerSelectPanel.transform, false);
            Button buttonComponent = newButton.GetComponent<Button>();
            newButton.transform.GetChild(0).GetComponent<Text>().text = "Layer " + i;
            int j = i;
            buttonComponent.onClick.AddListener(delegate () { GSBattleSceneManager.Instance.ChangeLayer(j); });
            buttonComponent.interactable = false;
            layerButtons.Add(buttonComponent);
        }
	}


    void OnActionStackChanged(object sender, EventArgs e)
    {
        undoButton.GetComponent<Button>().interactable = GSBattleSceneManager.Instance.UndoStack.Count > 0;
        redoButton.GetComponent<Button>().interactable = GSBattleSceneManager.Instance.RedoStack.Count > 0;
    }

    void OnGameStarted(object sender, EventArgs e)
    {
        startButton.interactable = false;
        foreach (Button button in layerButtons)
        {
            button.interactable = true;
        }
    }

    private void OnGameEnded(object sender, EventArgs e)
    {
        infoText.text = "Player " + (GSBattleSceneManager.Instance.CurrentPlayerNumber + 1) + " wins!";
    }

    private void OnTurnEnded(object sender, EventArgs e)
    {
        nextTurnButton.interactable = GSBattleSceneManager.Instance.CurrentPlayer is GSHumanPlayer;
        infoText.text = "Player " + (GSBattleSceneManager.Instance.CurrentPlayerNumber + 1);
    }

    private void OnTurnStarted(object sender, EventArgs e)
    {
        
    }

    private void OnLayerChanged(object sender, LayerChangedEvent e)
    {

    }



}
