using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// CellGrid class keeps track of the game, stores cells, units and players objects. It starts the game and makes turn transitions. 
/// It reacts to user interacting with units or cells, and raises events related to game progress. 
/// </summary>
public class GSCellGrid : MonoBehaviour
{
    public static GSCellGrid Instance { private set; get; }

    public List<Cell> Cells { get; private set; }
    public List<List<GSHex>> space;

    public int height = 10;

    public event EventHandler CellClicked;

    void Awake()
    {
        Instance = this;
    }

	void Start()
    {
    }


    public void StartLevel()
	{

        var gridGenerator = GSBattleSceneManager.Instance.GetComponent<ICellGridGenerator>();
        if (gridGenerator != null)
        {
            Cells = gridGenerator.GenerateGrid();
            foreach (var cell in Cells)
            {
                cell.CellClicked += OnCellClicked;
                ((GSHex)cell).MakeUnselectable();
            }
        }
        else
            Debug.LogError("No ICellGridGenerator script attached to cell grid");
    }
	

	private void OnCellClicked(object sender, EventArgs e)
	{
        CellClicked.Invoke(sender, e);
	}

    public void ChangeLayer(int oldLayer, int newLayer)
    {
        foreach(GSHex hex in space[oldLayer])
        {
            hex.MakeUnselectable();
        }
        
        foreach (GSHex hex in space[newLayer])
        {
            hex.MakeSelectable();
        }
    }
}
