﻿using UnityEngine;
using System.Collections;

public class GSGridMainCamera : MonoBehaviour {

    public static GSGridMainCamera Instance { get; private set; }

    private float MovementSpeed = 2;

    Vector3 currentDestination;
    Coroutine movementCoroutine;

    void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start() {
        GSBattleSceneManager.Instance.LayerChanged += OnLayerChanged;
        currentDestination = transform.position;
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnLayerChanged(object sender, LayerChangedEvent e){
        int levelDifference = e.newLayer - e.oldLayer;
        currentDestination = currentDestination + (Vector3.up * levelDifference * GSCommon.gridOffsetH);
        if (movementCoroutine != null)
            StopCoroutine(movementCoroutine);
        movementCoroutine = StartCoroutine(MovementAnimation(currentDestination));
    }

    protected virtual IEnumerator MovementAnimation(Vector3 destination)
    {
            while (transform.position != destination)
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * GSCommon.gridCameraMovementSpeed);
                yield return 0;
            }
        movementCoroutine = null;
    }
}
