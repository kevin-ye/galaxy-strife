class GSHumanPlayer : GSPlayer
{
	public override void Play(GSBattleSceneManager battleSceneManager)
	{
		battleSceneManager.GameState = new GSGameStateWaitingForInput(battleSceneManager);
	}
}