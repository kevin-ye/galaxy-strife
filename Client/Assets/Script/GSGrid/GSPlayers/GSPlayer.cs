using UnityEngine;

public abstract class GSPlayer : MonoBehaviour
{
	public int PlayerNumber;  
	/// <summary>
	/// Method is called every turn. Allows player to interact with his units.
	/// </summary>         
	public abstract void Play(GSBattleSceneManager battleSceneManager);
}