﻿using System.Collections.Generic;
using UnityEngine;

class GSCellGridGenerator : MonoBehaviour, ICellGridGenerator
{
    public Transform CellsParent;

    public List<GameObject> cellPrefabs;

    public int Height;
    public int Width;

    public List<List<GSHex>> space;



    /// <summary>
    /// dynamically generate cell prefabs
    /// </summary>

    public List<Cell> GenerateGrid()
    {
        HexGridType hexGridType = Width % 2 == 0 ? HexGridType.even_q : HexGridType.odd_q;
        List<Cell> hexagons = new List<Cell>();
        space = new List<List<GSHex>>();
        for(int i = 0; i < GSCommon.layers; i++)
        {
            space.Add(new List<GSHex>());
        }

        for (int i = 0; i < Height; i++)
        {
            for (int j = 0; j < Width; j++)
            {
                for (int k = 0; k < GSCommon.layers; k++)
                {
                    GameObject newHexagon = Instantiate(cellPrefabs[k]);
                    newHexagon.name = i + " : " + j + " : " + k;
                    Hexagon hexComponent = newHexagon.GetComponent<Hexagon>();
                    GSHex gsHexComponent = newHexagon.GetComponent<GSHex>();

                    newHexagon.transform.position = new Vector3((j * GSCommon.gridOffsetX) - (Width / 2 * GSCommon.gridOffsetX), k * GSCommon.gridOffsetH, (i * GSCommon.gridOffsetZ) + (j % 2 == 0 ? 0 : GSCommon.gridOffsetZ * 0.5f));
                    hexComponent.OffsetCoord = new Vector2(Width - j - 1, Height - i - 1);
                    hexComponent.HexGridType = hexGridType;
                    hexComponent.MovementCost = 1;
                    gsHexComponent.height = k;
                    hexagons.Add(newHexagon.GetComponent<Cell>());
                    space[k].Add(gsHexComponent);
                    newHexagon.transform.parent = CellsParent;
                    
                }
            }
        }
        GSCellGrid.Instance.space = space;
        foreach(Cell cell in hexagons)
        {
            GSHex hex = (GSHex)cell;
            hex.InitNeighbours(hexagons);
            
        }
        return hexagons;
    }
}

