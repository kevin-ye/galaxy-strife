using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GSUnit : Unit
{
	public Color PlayerColor;
    public int height;
    public string UnitName;
	
	private Transform Highlighter;

    
	
	public override void Initialize()
	{
		base.Initialize();
		SetColor(PlayerColor);
		
		Highlighter = transform.Find("Highlighter");
		if (Highlighter != null)
		{
			Highlighter.position = transform.position + new Vector3(0, 0, 1.5f);
			foreach (Transform cubeTransform in Highlighter)
				Destroy(cubeTransform.GetComponent<BoxCollider>());
		}     
		//gameObject.transform.position = Cell.transform.position + new Vector3(0, 0, -1.5f) + Vector3.back * (height - 1) * 2;
	}
	
	protected override void Defend(Unit other, int damage)
	{
		base.Defend(other, damage);
        
        UpdateHpBar();
	}
	
	public override void Move(Cell destinationCell, List<Cell> path)
	{
        GSHex hex = (GSHex)Cell;
        hex.RemoveUnit();
        Cell = destinationCell;
        hex = (GSHex)Cell;
        hex.SetUnit(this);
        Debug.Log(gameObject.transform.position);
        Debug.Log(destinationCell.transform.position);
        base.Move(destinationCell, path);
        Debug.Log(gameObject.transform.position);
	}
	
	public override void MarkAsAttacking(Unit other)
	{
		StartCoroutine(Jerk(other));
	}
	public override void MarkAsDefending(Unit other)
	{
		StartCoroutine(Glow(new Color(1, 0.5f, 0.5f), 1));
	}
	public override void MarkAsDestroyed()
	{
	}
	
	private IEnumerator Jerk(Unit other)
	{
		var heading = other.transform.position - transform.position;
		var direction = heading / heading.magnitude;
		float startTime = Time.time;
		
		while (startTime + 0.25f > Time.time)
		{
			transform.position = Vector3.Lerp(transform.position, transform.position + (direction / 2.5f), ((startTime + 0.25f) - Time.time));
			yield return 0;
		}
		startTime = Time.time;
		while (startTime + 0.25f > Time.time)
		{
			transform.position = Vector3.Lerp(transform.position, transform.position - (direction / 2.5f), ((startTime + 0.25f) - Time.time));
			yield return 0;
		}
		transform.position = Cell.transform.position;
	}
	private IEnumerator Glow(Color color, float cooloutTime)
	{
		float startTime = Time.time;
		
		while (startTime + cooloutTime > Time.time)
		{
			SetColor(Color.Lerp(PlayerColor, color, (startTime + cooloutTime) - Time.time));
			yield return 0;
		}
		
		SetColor(PlayerColor);
	}
	
	public override void MarkAsFriendly()
	{
		SetHighlighterColor(new Color(0.8f,1,0.8f));
	}
	public override void MarkAsReachableEnemy()
	{
		SetHighlighterColor(Color.red);
	}
	public override void MarkAsSelected()
	{
		SetHighlighterColor(new Color(0,1,0));
	}
	public override void MarkAsFinished()
	{
		SetColor(PlayerColor - Color.gray);
		SetHighlighterColor(new Color(0.8f, 1, 0.8f));
	}
	public override void UnMark()
	{
		SetColor(PlayerColor);
		SetHighlighterColor(Color.white);
		if (Highlighter == null) return;
		Highlighter.position = transform.position + new Vector3(0, 0, 1.52f);
	}
	
	public void UpdateHpBar()
	{
		if (GetComponentInChildren<Image>() != null)
		{
			GetComponentInChildren<Image>().transform.localScale = new Vector3((float)((float)HitPoints / (float)TotalHitPoints), 1, 1);
			GetComponentInChildren<Image>().color = Color.Lerp(Color.red, Color.green,
			                                                   (float)((float)HitPoints / (float)TotalHitPoints));
		}
	}
	private void SetColor(Color color)
	{
		GetComponent<Renderer>().material.color = color;
	}
	private void SetHighlighterColor(Color color)
	{
		
		if (Highlighter == null) return;
		Highlighter.position = transform.position + new Vector3(0, 0, 1.48f);
		for (int i = 0; i < Highlighter.childCount; i++)
		{
			var rendererComponent = Highlighter.transform.GetChild(i).GetComponent<Renderer>();
			if (rendererComponent != null)
				rendererComponent.material.color = color;
		}
	}

    public void MarkAsEnemy()
    {
        ((GSHex)Cell).MarkAsEnemy();
    }

    public override bool IsCellMovableTo(Cell cell)
    {
        return ((GSHex)cell).unit == null; 
    }

    public void UndoMove(GSHex hex, int pointsCost)
    {
        UnMark();
        MovementPoints += pointsCost;
        TeleportTo(hex);
    }

    public void RedoMove(GSHex hex, int pointsCost)
    {
        MovementPoints -= pointsCost;
        if (MovementPoints <= 0)
            MarkAsFinished();
        TeleportTo(hex);
    }

    private void TeleportTo(GSHex hex)
    {
        if (hex.IsTaken)
            throw new Exception("Unable to Teleport");
        ((GSHex)Cell).RemoveUnit();
        hex.SetUnit(this);
        Cell = hex;
        transform.position = Cell.transform.position;
    }

    public void UndoAttack(int pointsCost)
    {
        UnMark();
        ActionPoints += pointsCost;
    }

    public void RedoAttack(int pointsCost)
    {
        ActionPoints -= pointsCost;
        if (ActionPoints <= 0)
            MarkAsFinished();
    }

    protected override void OnDestroyed()
    {
        Cell.IsTaken = false;
        MarkAsDestroyed();
        gameObject.SetActive(false);
    }

    public void UndoDefend(GSUnit other)
    {
        HitPoints += Mathf.Clamp(other.AttackFactor- DefenceFactor, 1, other.AttackFactor);
        UpdateHpBar();
        if (HitPoints > 0)
        {
            gameObject.SetActive(true);
            ((GSHex)Cell).SetUnit(this);
        }


    }

    public void RedoDefend(GSUnit other)
    {
        HitPoints -= Mathf.Clamp(other.AttackFactor - DefenceFactor, 1, other.AttackFactor);
        UpdateHpBar();
        if (HitPoints <= 0)
            OnDestroyed();
    }

    public void MakeSelectable()
    {
        GetComponent<Collider>().enabled = true;
    }

    public void MakeUnselectable()
    {
        GetComponent<Collider>().enabled = false;
    }

    protected override IEnumerator MovementAnimation(List<Cell> path)
    {
        isMoving = true;
        path.Reverse();
        foreach (var cell in path)
        {
            while (transform.position != cell.transform.position)
            {
                transform.position = Vector3.MoveTowards(transform.position, cell.transform.position, Time.deltaTime * MovementSpeed);
                yield return 0;
            }
        }

        isMoving = false;
    }
}