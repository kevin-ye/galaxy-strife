using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

class GSUnitGenerator : MonoBehaviour, IUnitGenerator, ISerializationCallbackReceiver
{
	public Transform UnitsParent;

    private Dictionary<string,GameObject> unitsPrefabs;
    public List<UnitEntry> inspectorUnits;

    public static GSUnitGenerator Instance { private set; get; }

    static System.Random rnd = new System.Random();

    public void OnBeforeSerialize()
    {

    }

    public void OnAfterDeserialize()
    {
        unitsPrefabs = new Dictionary<string, GameObject>();
        foreach (UnitEntry entry in inspectorUnits)
        {
            unitsPrefabs.Add(entry.Name, entry.unit);
        }
    }


    /// <summary>
    /// Returns units that are already children of UnitsParent object.
    /// </summary>
    public List<Unit> SpawnUnits(List<Cell> cells)
	{
        generateUnit0(UnitsParent, cells);
        generateUnit1(UnitsParent, cells);
        List<Unit> ret = new List<Unit>();
		for (int i = 0; i < UnitsParent.childCount; i++)
		{
			var unit = UnitsParent.GetChild(i).GetComponent<GSUnit>();
            
			if(unit !=null)
			{
				GSHex cell = (GSHex) cells.OrderBy(h => Math.Abs((h.transform.localPosition - unit.transform.localPosition).magnitude)).First();
				if (cell.unit == null)
				{
					cell.IsTaken = true;
                    unit.Cell = cell;
					cell.SetUnit(unit);
                    unit.transform.position = cell.transform.position;
					unit.Initialize();
                    ret.Add(unit);
                }//Unit gets snapped to the nearest cell
				else
				{
                    Debug.Log("nearest cell already occupied");
					Destroy(unit.gameObject);
				}//If the nearest cell is taken, the unit gets destroyed.
			}
			else
			{
				Debug.LogError("Invalid object in Units Parent game object");
			}
			
		}
		return ret;
	}

    // dynamically generate unit prefabs for player0
    public void generateUnit0(Transform parent, List<Cell> cells)
    {
        for (int c = 0; c < 1; c++)
        {
            Cell hex = cells[rnd.Next(cells.Count)];
            while(((GSHex)hex).height != 1)
                hex = cells[rnd.Next(cells.Count)];
            GameObject unitClone = (GameObject)Instantiate(unitsPrefabs["spearman"]);
            GSUnit GSunitClone = unitClone.GetComponent<GSUnit>();
            GSunitClone.PlayerColor = Color.blue;
            GSunitClone.PlayerNumber = 0;
            unitClone.transform.SetParent(parent, true);
            unitClone.transform.position = hex.transform.position;
        }

        for (int c = 0; c < 1; c++)
        {
            Cell hex = cells[rnd.Next(cells.Count)];
            while (((GSHex)hex).height != 1)
                hex = cells[rnd.Next(cells.Count)];
            GameObject unitClone = (GameObject)Instantiate(unitsPrefabs["archer"]);
            GSUnit GSunitClone = unitClone.GetComponent<GSUnit>();
            GSunitClone.PlayerColor = Color.blue;
            GSunitClone.PlayerNumber = 0;
            unitClone.transform.SetParent(parent, true);
            unitClone.transform.position = hex.transform.position;
        }
    }

    // dynamically generate unit prefabs for player1
    public void generateUnit1(Transform parent, List<Cell> cells)
    {
        
        for (int c = 0; c < 1; c++)
        {
            Cell hex = cells[rnd.Next(cells.Count)];
            GameObject unitClone = (GameObject)Instantiate(unitsPrefabs["spearman"]);
            GSUnit GSunitClone = unitClone.GetComponent<GSUnit>();
            GSunitClone.PlayerColor = Color.red;
            GSunitClone.PlayerNumber = 1;
            unitClone.transform.SetParent(parent, true);
            unitClone.transform.position = hex.transform.position;
        }
    }



    [System.Serializable]
    public class UnitEntry
    {
        public string Name;
        public GameObject unit;
        public UnitEntry(string _name, GameObject _unit)
        {
            Name = _name;
            unit = _unit;
        }

    }

}



