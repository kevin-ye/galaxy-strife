﻿using UnityEngine;
using System.Collections;

public class GSActionAttack : GSAction {
    GSUnit _attacker, _defender;
    int _pointsCost;

    public GSActionAttack(GSUnit attacker, GSUnit defender, int pointsCost)
    {
        _attacker = attacker;
        _defender = defender;

    }
	public override GSGameState Undo()
    {
        _attacker.UndoAttack(_pointsCost);
        _defender.UndoDefend(_attacker);
        return new GSGameStateUnitSelected(GSBattleSceneManager.Instance, _attacker);
    }

    public override GSGameState Redo()
    {
        _attacker.RedoAttack(_pointsCost);
        _defender.RedoDefend(_attacker);
        return new GSGameStateUnitSelected(GSBattleSceneManager.Instance, _attacker);
    }
}
