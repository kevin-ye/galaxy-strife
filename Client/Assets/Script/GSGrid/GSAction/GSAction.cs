﻿using UnityEngine;
using System.Collections;

public abstract class GSAction
{
    public abstract GSGameState Redo();

    public abstract GSGameState Undo();

}
