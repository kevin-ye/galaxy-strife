﻿using UnityEngine;
using System.Collections;
using System;

public class GSActionMove : GSAction {
    public GSUnit _unit;
    public GSHex _oldHex, _newHex;
    public int _pointsCost;
    
    public GSActionMove(GSUnit unit, GSHex originalHex, GSHex newHex, int pointsCost)
    {
        _unit = unit;
        _oldHex = originalHex;
        _newHex = newHex;
        _pointsCost = pointsCost;
    }

    public override GSGameState Undo()
    {
        _unit.UndoMove(_oldHex, _pointsCost);
        return new GSGameStateUnitSelected(GSBattleSceneManager.Instance, _unit);
    }

    public override GSGameState Redo()
    {
        _unit.RedoMove(_newHex, _pointsCost);
        return new GSGameStateUnitSelected(GSBattleSceneManager.Instance, _unit);
    }
}
