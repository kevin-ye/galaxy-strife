﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class GSCommon
{
    public static readonly float gridOffsetX = 1.5f;
    public static readonly float gridOffsetZ = 1.75f;
    public static readonly float gridOffsetH = 7;

    public static readonly int layers = 3;
    public static readonly int initialLayer = 0;
    public static readonly float unselectableAlpha = 0.00f;

    public static readonly float gridCameraMovementSpeed = 3f;
}