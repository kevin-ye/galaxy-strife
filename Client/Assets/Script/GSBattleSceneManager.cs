﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GSBattleSceneManager : MonoBehaviour {

    public static GSBattleSceneManager Instance { private set; get; }

    public GameObject CellGridParent;
    public GameObject PlayersParent;
    
    public GameObject UnitsParent;

    [HideInInspector]
    public GSCellGrid CellGrid;
    [HideInInspector]
    public GSUnitManager UnitManager;

    public Stack<GSAction> UndoStack;
    public Stack<GSAction> RedoStack;

    private GSGameState _gameState;//The grid delegates some of its behaviours to cellGridState object.
    public GSGameState GameState
    {
        get
        {
            return _gameState;
        }
        set
        {
            Debug.Log("Changing states to " + value);
            if (_gameState != null)
                _gameState.OnStateExit();
            _gameState = value;
            _gameState.OnStateEnter();
        }
    }

    public List<Unit> selectableUnits;

    public event EventHandler ActionStackChanged;
    public event EventHandler<LayerChangedEvent> LayerChanged;
    public event EventHandler GameStarted;
    public event EventHandler GameEnded;
    public event EventHandler TurnEnded;
    public event EventHandler TurnStarted;

    public int currentLayer {
        get; private set;
    }

    public int NumberOfPlayers { get; private set; }

    public GSPlayer CurrentPlayer
    {
        get { return Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)); }
    }
    public int CurrentPlayerNumber { get; private set; }

    public List<GSPlayer> Players { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        TurnEnded += OnNextTurn;
        TurnStarted += OnTurnStarted;
        GameStarted += OnGameStarted;
        GameEnded += OnGameEnded;

        currentLayer = GSCommon.initialLayer;


    }

    // method to init a level, generate cells, grid, units
    public void StartLevel()
    {

        // generate new cells
        CellGrid = CellGridParent.GetComponent<GSCellGrid>();
        CellGrid.StartLevel();
        CellGrid.CellClicked += OnCellClicked;

        //generate new units
        UnitManager = UnitsParent.GetComponent<GSUnitManager>();
        UnitManager.StartLevel();
        UnitManager.UnitClicked += OnUnitClicked;
        UnitManager.UnitDestroyed += OnUnitDestroyed;

        Players = new List<GSPlayer>();
        for (int i = 0; i < PlayersParent.transform.childCount; i++)
        {
            var player = PlayersParent.transform.GetChild(i).GetComponent<GSPlayer>();
            if (player != null)
                Players.Add(player);
            else
                Debug.LogError("Invalid object in Players Parent game object");
        }
        NumberOfPlayers = Players.Count;
        CurrentPlayerNumber = Players.Min(p => p.PlayerNumber);

        // add events



        foreach (Transform unit in UnitsParent.transform)
        {
            unit.GetComponent<Unit>().UnitAttacked += OnUnitAttacked;
        }

        UndoStack = new Stack<GSAction>();
        RedoStack = new Stack<GSAction>();
        GameStarted.Invoke(this, new EventArgs());
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);
        ChangeLayer(currentLayer);
        

    }


    private void OnGameStarted(object sender, EventArgs e)
    {
        OnNextTurn(sender, e);
    }

    private void OnGameEnded(object sender, EventArgs e)
    {
       
    }

    private void OnTurnStarted(object sender, EventArgs e)
    {
        
    }

    private void OnNextTurn(object sender, EventArgs e)
    {
        UndoStack.Clear();
        ActionStackChanged.Invoke(this, new EventArgs());
    }

    private void OnCellClicked(object sender, EventArgs e)
    {
        
        GameState.OnCellClicked(sender as Cell);
    }

    private void OnUnitClicked(object sender, EventArgs e)
    {
        GameState.OnUnitClicked(sender as GSUnit);
    }

    private void OnUnitAttacked(object sender, AttackEventArgs e)
    {

    }

    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        Debug.Log("OnUnitDestroyed in Cell Grid");
        UnitManager.units.Remove(sender as GSUnit);
        var totalPlayersAlive = UnitManager.units.Select(u => u.PlayerNumber).Distinct().ToList(); //Checking if the game is over
        if (totalPlayersAlive.Count == 1)
        {
            if (GameEnded != null)
            {
                Debug.Log(CurrentPlayerNumber);
                GSBattleSceneManager.Instance.GameEnded.Invoke(this, new EventArgs());
            }
        }
    }

    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Undo()
    {
        if (UndoStack.Count == 0) {
            return;
        }
        GameState =     UndoStack.Peek().Undo();
        RedoStack.Push(UndoStack.Pop());
        ActionStackChanged.Invoke(this, new EventArgs());
    }

    public void Redo()
    {
        if (RedoStack.Count == 0) {
            return;
        }
        GameState = RedoStack.Peek().Redo();
        UndoStack.Push(RedoStack.Pop());
        ActionStackChanged.Invoke(this, new EventArgs());
    }

    public void AddToUndoStack(GSAction action)
    {
        Debug.Log("added to undo stack: " + action);
        UndoStack.Push(action);
        RedoStack.Clear();
        ActionStackChanged.Invoke(this, new EventArgs());
    }

    public void ChangeLayer(int layer)
    {
        int oldLayer = currentLayer;
        CellGrid.ChangeLayer(currentLayer, layer);
        currentLayer = layer;
        LayerChanged.Invoke(this, new LayerChangedEvent(oldLayer, currentLayer));
    }

    public void EndTurn()
    {
        Debug.Log("END");
        if (UnitManager.units.Select(u => u.PlayerNumber).Distinct().Count() == 1)
        {
            return;
        }
        GameState = new GSGameTurnChanging(this);

        UnitManager.units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnEnd(); });

        CurrentPlayerNumber = (CurrentPlayerNumber + 1) % NumberOfPlayers;
        while (UnitManager.units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).Count == 0)
        {
            CurrentPlayerNumber = (CurrentPlayerNumber + 1) % NumberOfPlayers;
        }//Skipping players that are defeated.
        Queue<GSAction> actionQueue = new Queue<GSAction>(UndoStack.Reverse());
        
        if (TurnEnded != null)
            TurnEnded.Invoke(this, new EventArgs());

        UnitManager.units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);
    }


}
