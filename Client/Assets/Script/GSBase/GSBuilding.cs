﻿using UnityEngine;
using System.Collections;

public abstract class GSBuilding : MonoBehaviour {

    public GameObject menuPrefab;
    public GameObject buttonPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        GenerateMenu();
    }

    protected abstract void GenerateMenu();
}
