﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GSGUIController : MonoBehaviour {

    public static GSGUIController Instance { private set; get; }

    public GameObject buildingMenu;
    public GameObject menuItems;


    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CloseBuildingMenu()
    {
        int count = menuItems.transform.childCount;
        for(int i = 0; i < count; i++)
        {
            Destroy(menuItems.transform.GetChild(i).gameObject);
        }
        buildingMenu.SetActive(false);
    }

    public void OpenBuildingMenu(GameObject menu)
    {
        
        menu.transform.SetParent(menuItems.transform,false);
        buildingMenu.SetActive(true);
    }
}
