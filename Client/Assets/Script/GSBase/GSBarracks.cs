﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class GSBarracks : GSBuilding {

    List<GSSquad> squads;

    protected override void GenerateMenu()
    {
        GameObject newMenu = Instantiate(menuPrefab) as GameObject;
        foreach (GSSquad squad in squads)
        {
            GameObject newButton = Instantiate(buttonPrefab,Vector3.zero,Quaternion.identity) as GameObject;
            newButton.transform.SetParent(newMenu.transform,false);
            newButton.GetComponent<Button>().onClick.AddListener(delegate() { MoveToBattle(squad); });
        }
        GSGUIController.Instance.OpenBuildingMenu(newMenu);
    }

    // Use this for initialization
    void Start () {
        squads = new List<GSSquad>();
        squads.Add(new GSSquad());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void MoveToBattle(GSSquad squad)
    {
        Application.LoadLevel("Map2");
    }

    
}
