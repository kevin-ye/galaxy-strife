﻿using System;

namespace GSNetwork
{
	public class GSPacket
	{
		public GSPacketType packetType {
			get;
			set;
		}

        protected virtual string getRawPayload()
        {
            return "";
        }

        public GSPacket()
        {
            packetType = GSPacketType.T_NULL;
        }

        public GSPacket(rawPacket raw)
        {
            // raw to GSPacket
            packetType = raw.packetType;
        }

        public virtual rawPacket getRaw()
        {
            rawPacket ret = new rawPacket();

            // GSPacket to raw
            ret.packetType = GSPacketType.T_NULL;
            ret.payload = getRawPayload();

            return ret;
        }

    }
}

