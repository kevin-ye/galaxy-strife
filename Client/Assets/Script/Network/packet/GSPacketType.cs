﻿namespace GSNetwork
{
    public enum GSPacketType 
	{
        T_LOGIN,
        T_BATTLE,
        T_MATCHMAKING,

        T_CLOSE,

        T_ERR,
        T_TIMEOUT,
        T_NULL
    }
}

