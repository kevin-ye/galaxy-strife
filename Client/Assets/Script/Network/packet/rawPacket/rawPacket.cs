﻿using System;

namespace GSNetwork
{
    [Serializable()]
    public class rawPacket
    {
        // fields must be public in order to serialize as xml string
        public GSPacketType packetType = GSPacketType.T_NULL;

        public string payload;

        public rawPacket() { }
    }
}
