﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSNetwork
{
    [Serializable()]
    public class loginPayload : packetPayload
    {
        public string uid;
        public int status_code;
    }
}
