﻿using System;

namespace GSNetwork
{
    public static class GSNetworkCommon
    {
        // get login string
        public static rawPacket getLoginPacket()
        {
            rawPacket loginPacket = new rawPacket();

            loginPacket.packetType = GSPacketType.T_LOGIN;

            return loginPacket;
        }

        // server const
        public static readonly string serverIP = "192.99.14.46";
        public static readonly int serverPort = 35565;

        // other const
        public static readonly int defaultTimeout = 5000;  // 5s max timeout
        public static readonly string packetEndCondition = "</rawPacket>";

        // call when encounter an unexpected exception in network_handler
        public static void Panic(Exception e)
        {
            // panic
            Console.WriteLine(e.ToString());
        }
    }
}
