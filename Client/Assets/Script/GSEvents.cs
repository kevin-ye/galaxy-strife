﻿using UnityEngine;
using System.Collections;
using System;

public class LayerChangedEvent: EventArgs {
        readonly public int oldLayer, newLayer;
        public LayerChangedEvent(int _oldLayer, int _newLayer){
            oldLayer = _oldLayer;
            newLayer = _newLayer;
        }
}
