#pragma once

#include <cstdlib>
#include <cstdio>
#include <iostream>

enum class packetType {
	T_NULL,

        T_LOGIN,
        T_BATTLE,
        T_MATCHMAKING,

        T_CLOSE,

        T_ERR,
        T_TIMEOUT,
        T_NONE
};

class xmlPacket
{
	std::string xmlstring;
	
	// packet data
	packetType _type;

public:
        xmlPacket();
	xmlPacket(std::string xmlstring);
	~xmlPacket();

	std::string getXML();

	packetType getType();
};
