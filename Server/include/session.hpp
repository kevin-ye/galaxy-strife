#pragma once

#include <cstdlib>
#include <cstdio>
#include <memory>
#include <mutex>
#include <queue>
#include <boost/asio.hpp>

class xmlPacket;
class serverControl;
class battleRoom;

class session : public std::enable_shared_from_this<session>
{
	std::mutex outgoingPktMutex;
	std::queue<xmlPacket> outgoingPkts;
	boost::asio::ip::tcp::socket _socket;
	serverControl *sControl;
	std::shared_ptr<battleRoom> room;
	boost::asio::streambuf *readbuf;

public:
	session(boost::asio::io_service &_io_service, serverControl *s);
	~session();

	void start();
	void handle_read(const boost::system::error_code& error);
	void handle_write(const boost::system::error_code& error);

	// respond
	void deliver(xmlPacket pkt);
	// close connection
	void closeConnection();

	boost::asio::ip::tcp::socket &getSocket();
};
