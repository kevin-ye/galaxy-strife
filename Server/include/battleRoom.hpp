#pragma once

#include <cstdlib>
#include <cstdio>
#include <set>
#include <queue>
#include <boost/thread.hpp>
#include <mutex>
#include <thread>

#include "semaphore.hpp"
#include "requestInfo.hpp"

class xmlPacket;
class session;

class battleRoom : public std::enable_shared_from_this<battleRoom>
{
	std::set<std::shared_ptr<session>> participants;

	std::queue<requestInfo> requestQueue;
	std::mutex requestQueueMutex;
	semaphore requestQueueSemaphore;
	std::thread *_requestHandlerThread;
	bool threadTerminate;

	void startRequestHandlerThread();
	void signalTermination();
	void threadMain();

public:
	battleRoom();
	~battleRoom();

	void forceToShutdown(xmlPacket &msg);

	void join(std::shared_ptr<session> newParticipant);
	void leave(std::shared_ptr<session> Participant);

	void addRequest(xmlPacket &r, std::shared_ptr<session> sender);
};
