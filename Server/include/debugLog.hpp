#pragma once

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream> 

class NullStream : public std::streambuf
{
public:
	int overflow(int c);
};

class debugLog
{
	static NullStream voidstream;

	static std::fstream stdoutstream;
	static std::fstream stderrstream;

public:

	static void init();

	#ifdef DISABLEDEBUGLOG

	static NullStream &stdout();
	static NullStream &stderr();

	#else

	static std::fstream &stdout();
	static std::fstream &stderr();

	#endif
};