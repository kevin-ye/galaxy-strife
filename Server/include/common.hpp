#pragma once

#include <cstdlib>
#include <iostream>

const short DefaultPort = 35565;
const std::string configFilePath = "config/config.xml";
const std::string stdoutLogFilePath = "Logs/Logs.log";
const std::string stderrLogFilePath = "Logs/Errors.log";

// end condition of xml string sent from client
const std::string packetEndCondition = "</rawPacket>";