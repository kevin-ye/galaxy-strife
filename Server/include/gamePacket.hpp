#pragma once

#include <cstdlib>
#include <iostream>

class gamePacket
{
	bool valid;
	std::string xmlString;

	void ParseXML();

public:
	gamePacket(std::string payload);
	~gamePacket();

	bool getValid();
	std::string getPayload();

	/* data */
};