#pragma once

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <stack>
#include <exception>
#include <boost/thread/mutex.hpp>

class customException : std::exception
{
	std::string msg;
public:
	customException(std::string _msg);
	const char* what() const noexcept;
};

class exceptionHandler
{
	exceptionHandler();

	std::stack<std::exception_ptr> exceptionStack;
	static boost::mutex exceptionStack_mutex;

public:

	~exceptionHandler();
	static exceptionHandler &getInstance();
	exceptionHandler(exceptionHandler const&) = delete;
	exceptionHandler& operator=(exceptionHandler const&) = delete;

	void pushException(std::exception_ptr e);
	std::exception_ptr popException();

};
