#pragma once

#include <cstdlib>
#include <cstdio>
#include <memory>
#include <queue>
#include <mutex>
#include <boost/asio.hpp>
#include <thread>
#include "semaphore.hpp"
#include "requestInfo.hpp"

class session;
class battleRoom;
class xmlPacket;

class serverControl
{
	boost::asio::io_service *_io_service;
	boost::asio::ip::tcp::acceptor *_acceptor;

	void start_accept();
	void handle_accept(std::shared_ptr<session> se, const boost::system::error_code& err);

	std::shared_ptr<battleRoom> createBattleRoom();

	std::queue<requestInfo> requestQueue;
	std::mutex requestQueueMutex;
	semaphore requestQueueSemaphore;
	std::thread *_requestHandlerThread;
	bool threadTerminate;

	void startRequestHandlerThread();
	void signalTermination();
	void threadMain();

public:
	serverControl();
	~serverControl();

	void start();
	void shutdown();

	void addRequest(xmlPacket &r, std::shared_ptr<session> sender);
};
