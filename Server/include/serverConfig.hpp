#pragma once

#include <cstdlib>
#include "debugLog.hpp"
#include <boost/thread/mutex.hpp>

// singleton
class serverConfig
{
	serverConfig();
	static boost::mutex s_mutex;

	// server config fields
	int _port;
	std::string filePath;

public:

	~serverConfig();

	static serverConfig &getInstance();
	serverConfig(serverConfig const&) = delete;
	serverConfig& operator=(serverConfig const&) = delete;

	void load();
	void reload();

	int getPort();
	
};