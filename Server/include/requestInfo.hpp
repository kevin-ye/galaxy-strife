#pragma once

#include <memory>
#include "xmlPacket.hpp"

class session;

class requestInfo
{
public:
        requestInfo();
        ~requestInfo();
	xmlPacket packet;
	std::shared_ptr<session> sender;
};
