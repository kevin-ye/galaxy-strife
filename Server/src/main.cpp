#include "common.hpp"
#include "serverConfig.hpp"
#include "debugLog.hpp"

#include <iostream>

#include "serverControl.hpp"

using namespace std;

void serverInit()
{
	debugLog::init();
	serverConfig::getInstance().load();
}

int main(int argc, char** argv) 
{
	serverInit();

	try
	{
		serverControl mainControl;
		mainControl.start();
	}
	catch (exception &e)
	{
		debugLog::stderr() << e.what() << endl;
	}

	return 0;
}