#include "session.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "xmlPacket.hpp"
#include "battleRoom.hpp"
#include "exceptionHandler.hpp"
#include "common.hpp"
#include "serverControl.hpp"

using namespace std;

session::session(boost::asio::io_service &_io_service, serverControl *s) 
: _socket(_io_service), sControl(s), room(NULL), readbuf(NULL),
outgoingPkts(std::queue<xmlPacket>())
{}

session::~session() 
{}

void session::start()
{
	readbuf = new boost::asio::streambuf();
	boost::asio::async_read_until(_socket, *readbuf, packetEndCondition,
		boost::bind(&session::handle_read, shared_from_this(),
			boost::asio::placeholders::error));
}

void session::handle_read(const boost::system::error_code& error)
{
	try 
	{
		if (error)
		{
			throw customException(error.message());
		}
		istream is(readbuf);
		string xmlstring(istreambuf_iterator<char>(is), {});
		// string to packet
		xmlPacket incomingPacket(xmlstring);
		if (incomingPacket.getType() == packetType::T_BATTLE)
		{
			// packet is a battle type
			if (!room)
			{
				// but there is no assigned room
				// error
				throw customException("No room is assigned to this session");
			}
		} else if (incomingPacket.getType() != packetType::T_CLOSE) {
			// general request
			// sender to serverControl
			sControl->addRequest(incomingPacket, shared_from_this());
			// read more packets
			delete readbuf;
			readbuf = new boost::asio::streambuf();
			boost::asio::async_read_until(_socket, *readbuf, packetEndCondition,
				boost::bind(&session::handle_read, shared_from_this(),
					boost::asio::placeholders::error));
		} else {
			// otherwise is a T_CLOSE packet
			// or an unknown packet type (error?)
			closeConnection();
		}
	} 
	catch (...) 
	{
		exceptionHandler::getInstance().pushException(current_exception());
	}
}

void session::closeConnection()
{
	if (room)
	{
		room->leave(shared_from_this());
		room = NULL;
	}
	// then?
}

void session::handle_write(const boost::system::error_code& error)
{
	try 
	{
		if (error)
		{
			throw customException(error.message());
		}
		// acquire lock
		std::lock_guard<std::mutex> guardlock(outgoingPktMutex);

		if (outgoingPkts.size() == 0)
		{
			return;
		}

		xmlPacket packet = outgoingPkts.front();
		outgoingPkts.pop();
		string payload = packet.getXML();

		// send next
		boost::asio::async_write(_socket, 
			boost::asio::buffer(payload.c_str(), payload.length() + 1),
			boost::bind(&session::handle_write, shared_from_this(), boost::asio::placeholders::error));
	} 
	catch(...)
	{
		exceptionHandler::getInstance().pushException(current_exception());
	}
}

void session::deliver(xmlPacket pkt)
{

	// acquire lock to push
	{
		std::lock_guard<std::mutex> guardlock(outgoingPktMutex);
		outgoingPkts.push(pkt);
	}

	// acquire lock to get first packet to send
	xmlPacket packet;
	{
		std::lock_guard<std::mutex> guardlock(outgoingPktMutex);
		if (outgoingPkts.size() == 0)
		{
			return;
		}
		packet = outgoingPkts.front();
		outgoingPkts.pop();
	}
	string payload = packet.getXML();
        boost::asio::async_write(_socket, 
		boost::asio::buffer(payload.c_str(), payload.length() + 1),
		boost::bind(&session::handle_write, shared_from_this(), boost::asio::placeholders::error));
}

boost::asio::ip::tcp::socket &session::getSocket()
{
	return _socket;
}
