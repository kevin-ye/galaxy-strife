#include "debugLog.hpp"
#include "common.hpp"

#include <iostream>
#include <fstream>
#include <cstdio>
#include <chrono>
#include <iomanip>

using namespace std;

NullStream debugLog::voidstream;
fstream debugLog::stdoutstream;
fstream debugLog::stderrstream;

int NullStream::overflow(int c)
{
	return c;
}

void debugLog::init()
{
	remove(stdoutLogFilePath.c_str());
	remove(stderrLogFilePath.c_str());

	// init streams
	#ifdef DISABLEDEBUGLOG
	#else
	debugLog::stdoutstream.open(stdoutLogFilePath, std::fstream::out | std::fstream::app);
	debugLog::stderrstream.open(stderrLogFilePath, std::fstream::out | std::fstream::app);
	#endif
}

#ifdef DISABLEDEBUGLOG

NullStream &debugLog::stdout()
{
	return debugLog::voidstream;
}

NullStream &debugLog::stderr()
{
	return debugLog::voidstream;
}
#else

fstream &debugLog::stdout()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	auto duration = now.time_since_epoch();
	auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
    duration -= seconds;
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
	duration -= milliseconds;
	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(duration);
	duration -= microseconds;
	auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);

    debugLog::stdoutstream << "<" 
    << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") 
    << "." << milliseconds.count()
    << "." << microseconds.count()
    << "." << nanoseconds.count()
    << ">: ";
	return debugLog::stdoutstream;
}
fstream &debugLog::stderr()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	auto duration = now.time_since_epoch();
	auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
    duration -= seconds;
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
	duration -= milliseconds;
	auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(duration);
	duration -= microseconds;
	auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);

    debugLog::stderrstream << "<" 
    << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") 
    << "." << milliseconds.count()
    << "." << microseconds.count()
    << "." << nanoseconds.count()
    << ">: ";
	return debugLog::stderrstream;
}

#endif
