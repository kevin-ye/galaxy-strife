#include "xmlPacket.hpp"
#include "common.hpp"
#include "debugLog.hpp"

#include <pugixml.hpp>

using namespace std;
using namespace pugi;

xmlPacket::xmlPacket()
: xmlstring("")
{
	_type = packetType::T_NONE;
}

xmlPacket::xmlPacket(string xmlstring)
: xmlstring(xmlstring)
{
	// when parsing incorrectly, this packet is T_ERR
	_type = packetType::T_ERR;

	xml_document doc;
    xml_parse_result result = doc.load_string(xmlstring.c_str());

    if (result)
    {
    	string typeString = doc.child("rawPacket").child_value("packetType");
    	if (typeString == "T_NULL")
    	{
    		_type = packetType::T_NULL;
    	} else if (typeString == "T_LOGIN")
    	{
    		_type = packetType::T_LOGIN;
    	} else if (typeString == "T_BATTLE")
    	{
    		_type = packetType::T_BATTLE;
    	} else if (typeString == "T_MATCHMAKING")
    	{
    		_type = packetType::T_MATCHMAKING;
    	} else if (typeString == "T_CLOSE")
    	{
    		_type = packetType::T_CLOSE;
    	} else if (typeString == "T_ERR")
    	{
    		_type = packetType::T_ERR;
    	} else if (typeString == "T_TIMEOUT")
    	{
    		_type = packetType::T_TIMEOUT;
    	} else if (typeString == "T_NONE")
    	{
    		_type = packetType::T_NONE;
    	}
    }

}

xmlPacket::~xmlPacket()
{}

string xmlPacket::getXML()
{
	return xmlstring;
}

packetType xmlPacket::getType()
{
	return _type;
}
