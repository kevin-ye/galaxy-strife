#include "serverConfig.hpp"
#include "common.hpp"

#include <cstdio>
#include <pugixml.hpp>
#include <iostream>
#include <fstream>
#include <boost/thread/locks.hpp> 
#include <boost/thread/lock_guard.hpp>

using namespace std;
using namespace pugi;

boost::mutex serverConfig::s_mutex;

serverConfig::serverConfig()
:_port(DefaultPort),
filePath(configFilePath)
{}

serverConfig::~serverConfig() {}

serverConfig &serverConfig::getInstance()
{
	static serverConfig staticInstance;

	return staticInstance;
}

void serverConfig::load()
{
	boost::lock_guard<boost::mutex> lock(s_mutex);
	
	// load config.xml
	ifstream configFile(configFilePath);

	xml_document configXML;
	xml_parse_result result = configXML.load_file(configFilePath.c_str());

	if (result)
	{
		_port = atoi(configXML.child("Config").child("Settings").attribute("port").value());
	} else {
		// load default
		_port = DefaultPort;
	}

}

void serverConfig::reload()
{
	load();
}

int serverConfig::getPort()
{
	return _port;
}