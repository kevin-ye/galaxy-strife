#include "battleRoom.hpp"

#include <algorithm>
#include <mutex>
#include <boost/asio.hpp>

#include "xmlPacket.hpp"
#include "session.hpp"
#include "requestInfo.hpp"
#include "semaphore.hpp"

using namespace std;

battleRoom::battleRoom()
: participants(set<std::shared_ptr<session>>()),
requestQueue(queue<requestInfo>()),
_requestHandlerThread(NULL),
threadTerminate(false)
{
	startRequestHandlerThread();
}

battleRoom::~battleRoom()
{
	if (!_requestHandlerThread && _requestHandlerThread->joinable())
	{
		signalTermination();
		_requestHandlerThread->join();
	}

	delete _requestHandlerThread;
}

void battleRoom::startRequestHandlerThread()
{
	threadTerminate = false;
	_requestHandlerThread = new std::thread(&battleRoom::threadMain, this);
}

void battleRoom::signalTermination()
{
	threadTerminate = true;
	// unblock the possible thread on requestQueueSemaphore
	requestQueueSemaphore.V();
}

void battleRoom::threadMain()
{
	// request handler
	while (!threadTerminate)
	{
		requestQueueSemaphore.P();
		if (threadTerminate)
		{
			break;
		}
		requestInfo topRequest;
		{
			std::lock_guard<std::mutex> guardlock(requestQueueMutex);
			if (requestQueue.empty())
			{
				continue;
			}
			requestInfo topRequest = requestQueue.front();
			requestQueue.pop();
		}

		// process this request
		xmlPacket respond;

		// *** echo for now ***
		respond = topRequest.packet;

		//deliver to all participants
		std::for_each(participants.begin(), participants.end(), 
			[respond](std::shared_ptr<session> thisSession){
				thisSession->deliver(respond);
			});
	}
}

void battleRoom::forceToShutdown(xmlPacket &msg)
{
	signalTermination();
	// tell all participants ?
}

void battleRoom::join(std::shared_ptr<session> newParticipant)
{
	participants.insert(newParticipant);
	// tell all participants ?
}

void battleRoom::leave(std::shared_ptr<session> Participant)
{
	participants.erase(Participant);
	// tell all participants ?
}

void battleRoom::addRequest(xmlPacket &r, std::shared_ptr<session> sender)
{
	// add battle request
	requestInfo newinfo;
	newinfo.packet = r;
	newinfo.sender = sender;
	{
		std::lock_guard<std::mutex> guardlock(requestQueueMutex);
		requestQueue.push(newinfo);
	}
	requestQueueSemaphore.V();
}

