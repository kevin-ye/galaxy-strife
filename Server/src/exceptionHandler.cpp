#include "exceptionHandler.hpp"
#include "common.hpp"
#include "debugLog.hpp"

#include <boost/thread/locks.hpp> 
#include <boost/thread/lock_guard.hpp> 

using namespace std;

boost::mutex exceptionHandler::exceptionStack_mutex;

// custom exceptions

customException::customException(string _msg) : msg(_msg) 
{}

const char* customException::what() const noexcept
{
	return msg.c_str();
}

//////////////////////////////////////////////////////////////////////

exceptionHandler::exceptionHandler()
: exceptionStack(std::stack<std::exception_ptr>())
{}

exceptionHandler::~exceptionHandler()
{}

exceptionHandler &exceptionHandler::getInstance()
{
	static exceptionHandler exceptionHandlerInstance;

	return exceptionHandlerInstance;
}

void exceptionHandler::pushException(exception_ptr e)
{
	boost::lock_guard<boost::mutex> lock(exceptionStack_mutex);

	exceptionStack.push(e);
}

exception_ptr exceptionHandler::popException()
{
	boost::lock_guard<boost::mutex> lock(exceptionStack_mutex);

	if (!exceptionStack.empty())
	{
		exception_ptr retptr = exceptionStack.top();
		exceptionStack.pop();
		
		return retptr;
	}
}
