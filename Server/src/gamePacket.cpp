#include "gamePacket.hpp"

#include "common.hpp"

using namespace std;

gamePacket::gamePacket(std::string payload) : xmlString(payload), valid(false)
{
	ParseXML();
}

gamePacket::~gamePacket() {}

void gamePacket::ParseXML()
{
	valid = false;
	//
	valid = true;
}

bool gamePacket::getValid()
{
	return valid;
}

string gamePacket::getPayload()
{
	return xmlString;
}