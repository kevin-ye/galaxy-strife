#include "serverControl.hpp"
#include "common.hpp"
#include "debugLog.hpp"
#include "exceptionHandler.hpp"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iterator>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

#include "xmlPacket.hpp"
#include "battleRoom.hpp"
#include "serverConfig.hpp"
#include "requestInfo.hpp"
#include "semaphore.hpp"
#include "session.hpp"

using namespace std;
using boost::asio::ip::tcp;

serverControl::serverControl()
: _io_service(NULL),
_acceptor(NULL), 
_requestHandlerThread(NULL),
threadTerminate(false),
requestQueue(std::queue<requestInfo>())
{}

serverControl::~serverControl()
{
	if (!_requestHandlerThread && _requestHandlerThread->joinable())
	{
		signalTermination();
		_requestHandlerThread->join();
	}

	delete _requestHandlerThread;
        delete _io_service;
        delete _acceptor;
}

shared_ptr<battleRoom> serverControl::createBattleRoom()
{
	shared_ptr<battleRoom> theRoom(new battleRoom());

	return theRoom;
}

void serverControl::start_accept()
{
	std::shared_ptr<session> connectionSession(new session(*_io_service, this));
	_acceptor->async_accept(connectionSession->getSocket(), 
		boost::bind(&serverControl::handle_accept, this, connectionSession, boost::asio::placeholders::error));
}

void serverControl::handle_accept(std::shared_ptr<session> se, const boost::system::error_code& err)
{
	if (!err)
	{
		// accept connection and start it
		se->start();
	}

	// accept new connections
	start_accept();
}

void serverControl::addRequest(xmlPacket &r, std::shared_ptr<session> sender)
{
	// add general request
	requestInfo newinfo;
	newinfo.packet = r;
	newinfo.sender = sender;
	{
		std::lock_guard<std::mutex> guardlock(requestQueueMutex);
		requestQueue.push(newinfo);
	}
	requestQueueSemaphore.V();
}

void serverControl::startRequestHandlerThread()
{
	threadTerminate = false;
	_requestHandlerThread = new std::thread(&serverControl::threadMain, this);
}

void serverControl::threadMain()
{
	// request handler
	while (!threadTerminate)
	{
		requestQueueSemaphore.P();
		if (threadTerminate)
		{
			break;
		}
		requestInfo topRequest;
		{
			std::lock_guard<std::mutex> guardlock(requestQueueMutex);
			if (requestQueue.empty())
			{
				continue;
			}
			topRequest = requestQueue.front();
			requestQueue.pop();
		}

		// process this request
		xmlPacket respond;

		// *** echo for now ***
		respond = topRequest.packet;

		// deliver back to sender
		topRequest.sender->deliver(respond);
	}
}

void serverControl::signalTermination()
{
	threadTerminate = true;
	// unblock the possible thread on requestQueueSemaphore
	requestQueueSemaphore.V();
}

void serverControl::shutdown()
{
	signalTermination();
}

void serverControl::start()
{
    _io_service = new boost::asio::io_service();
	_acceptor = new tcp::acceptor(*_io_service, tcp::endpoint(tcp::v4(), 
                                                  serverConfig::getInstance().getPort()));
	startRequestHandlerThread();
	try
	{
		start_accept();
		_io_service->run();
	} 
	catch (...) {
		exceptionHandler::getInstance().pushException(current_exception());
	}
}
