#!lua

includeDirList = {
    "include",
    "lib/pugixml-1.7/src/"
}

libDirectories = { 
    "lib"
}

linkLibs = {
    "stdc++",
    "pthread",
    "boost_system",
    "boost_thread"
}

buildOptions = {"-std=c++11"}

solution "BuildStaticLibs"
    configurations { "Debug", "Release" }

    project "ServerControl"
        kind "ConsoleApp"
        language "C++"
        location "build"
        objdir "build"
        targetdir "."
        buildoptions (buildOptions)
        libdirs (libDirectories)
        links (linkLibs)
        linkoptions (linkOptionList)
        includedirs (includeDirList)
        files 
        {   "src/*.cpp", 
            "lib/pugixml-1.7/src/*.cpp"
        }

    configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }

    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }
