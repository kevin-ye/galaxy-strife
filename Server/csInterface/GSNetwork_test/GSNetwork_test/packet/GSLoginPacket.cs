﻿using System.Xml.Serialization;
using System.Text;
using System.IO;

namespace GSNetwork
{
    public class GSLoginPacket : GSPacket
    {
        public string uid;
        public int status_code;
        public int base_id;

        public GSLoginPacket()
        {
            packetType = GSPacketType.T_LOGIN;
        }

        public GSLoginPacket(rawPacket raw)
        {
            // raw to GSLoginPacket
            packetType = GSPacketType.T_LOGIN;

            // deserializer payload
            loginPayload thepayload = new loginPayload();
            StringReader sreader = new StringReader(raw.payload);
            XmlSerializer serializer = new XmlSerializer(thepayload.GetType());
            thepayload = (loginPayload)serializer.Deserialize(sreader);

            uid = thepayload.uid;
            status_code = thepayload.status_code;
            base_id = thepayload.base_id;

        }

        protected override string getRawPayload()
        {
            loginPayload thepayload = new loginPayload();
            thepayload.uid = uid;
            thepayload.status_code = 0;
            thepayload.base_id = 0;

            XmlSerializer serializer = new XmlSerializer(thepayload.GetType());
            StringBuilder builder = new StringBuilder();
            StringWriter writer = new StringWriter(builder);
            serializer.Serialize(writer, thepayload);

            return builder.ToString();
        }

        public override rawPacket getRaw()
        {
            rawPacket ret = new rawPacket();

            // GSLoginPacket to raw
            ret.packetType = GSPacketType.T_LOGIN;
            ret.payload = getRawPayload();

            return ret;
        }
    }
}
