﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSNetwork
{
    [Serializable()]
    public class matchMakingPayload : packetPayload
    {
        public string uid;
        public string opponent_uid;
    }
}
