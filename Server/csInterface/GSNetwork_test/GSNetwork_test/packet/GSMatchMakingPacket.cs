﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSNetwork
{
    public class GSMatchMakingPacket : GSPacket
    {
        public string uid;
        public string opponent_uid;

        public GSMatchMakingPacket()
        {
            packetType = GSPacketType.T_MATCHMAKING;
        }

        protected override string getRawPayload()
        {
            return base.getRawPayload();
        }

        public GSMatchMakingPacket(rawPacket raw)
        {
            // raw to GSLoginPacket
            packetType = GSPacketType.T_MATCHMAKING;
        }

        public override rawPacket getRaw()
        {
            rawPacket ret = new rawPacket();
            matchMakingPayload payload = new matchMakingPayload();

            // GSLoginPacket to raw
            ret.packetType = GSPacketType.T_MATCHMAKING;
            payload.uid = uid;
            payload.opponent_uid = opponent_uid;
            ret.payload = getRawPayload();

            return ret;
        }
    }
}
