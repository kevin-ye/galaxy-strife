﻿using System;
using System.Threading;
using System.Collections;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.IO;

namespace GSNetwork
{
    public class network_handler
	{
        public delegate void LoginCallbackDelegate(GSLoginPacket packet);
        public delegate void MatchMakingCallbackDelegate(GSMatchMakingPacket packet);


        public delegate void recieveDelegate(GSPacket packet);

        private static network_handler _instance = null;
		private Thread _thread = null;
		private bool threadEnd = true;
		private Queue sendingQueue = new Queue();

        // callback delegates
        private LoginCallbackDelegate LoginCallback = null;
        private MatchMakingCallbackDelegate MatchMakingCallback = null;
        private recieveDelegate SubmitMovesCallback = null;
        private recieveDelegate BaseRequestCallback = null;
        private recieveDelegate GenericSendCallback = null;
        private recieveDelegate ErrorCallback = null;
        private recieveDelegate TimeoutCallback = null;

        #region singleton

        private void init()
		{
			//
			sendingQueue.Clear();
		}

		private network_handler ()
		{
			// cstr
			init();
		}

		~network_handler() 
		{
			// dstr
			if ((_thread != null) && (threadEnd == false)) {
				Terminate ();
				_thread.Join ();
			}
		}

		public static network_handler Instance
		{
			get {

				if (_instance == null) {
					// first init
					// create a new instance
					_instance = new network_handler();
				}

				return _instance;
			}
		}

        #endregion

        #region register callbacks

        public void registerLoginCallback(LoginCallbackDelegate _del)
        {
            LoginCallback = _del;
        }

        public void registerMatchMakingCallback(MatchMakingCallbackDelegate _del)
        {
            MatchMakingCallback = _del;
        }

        public void registerSubmitMovesCallback(recieveDelegate _del)
        {
            SubmitMovesCallback = _del;
        }

        public void registerBaseRequestCallback(recieveDelegate _del)
        {
            BaseRequestCallback = _del;
        }

        public void registerGenericSendCallback(recieveDelegate _del)
        {
            GenericSendCallback = _del;
        }

        public void registerErrorCallback(recieveDelegate _del)
        {
            ErrorCallback = _del;
        }

        public void registerTimeoutCallback(recieveDelegate _del)
        {
            TimeoutCallback = _del;
        }

        #endregion

        #region sending
        public void login(string uid)
        {
            // form a login packet
            GSLoginPacket pkt = new GSLoginPacket();
            pkt.packetType = GSPacketType.T_LOGIN;
            pkt.uid = uid;
            addToSendingQueue(pkt);
        }

        public void logout()
        {
            // close connection
            // send a T_CLOSE packet to notify server
            GSPacket closepkt = new GSPacket();
            closepkt.packetType = GSPacketType.T_CLOSE;
            addToSendingQueue(closepkt);
        }

        public void Match()
        {
            // find a match
        }

        public void SubmitMoves(GSPacket moves)
        {
            // end turn
        }

        public void RequestBaseInfo(GSPacket request)
        {
            //
        }

        public void GenericSend(GSPacket packet)
        {
            //
        }

        private void addToSendingQueue(GSPacket packet)
        {
            lock(sendingQueue)
            {
                sendingQueue.Enqueue(packet);
                Monitor.Pulse(sendingQueue);
            }
        }

        #endregion

        #region thread control

        private void threadMain()
		{
            // handler main()
            // create TCP connection
            TcpClient clientConnection = null;
            try
            {
                clientConnection = new TcpClient(GSNetworkCommon.serverIP, GSNetworkCommon.serverPort);
            }
            catch (SocketException e)
            {
                // connection error
                // Console.WriteLine(e.ToString());
                if (ErrorCallback != null) ErrorCallback(new GSPacket());
                return;
            }

            NetworkStream netStream = clientConnection.GetStream();
            netStream.Flush();
			while (!threadEnd) {
                // main loop
                GSPacket packet = null;
                rawPacket recievedRaw = new rawPacket();
                try
                {
                    lock(sendingQueue)
                    {
                        while (sendingQueue.Count <= 0)
                        {
                            // wait until there is send request
                            Monitor.Wait(sendingQueue);
                        }
                        packet = (GSPacket)sendingQueue.Dequeue();
                    }
                    // send through the network stream
                    rawPacket raw = packet.getRaw();
                    XmlSerializer serializer = new XmlSerializer(raw.GetType());
                    serializer.Serialize(netStream, raw);
                    netStream.Flush();
                    if (packet.packetType == GSPacketType.T_CLOSE)
                    {
                        break;
                    }
                    // recieve server respond
                    netStream.ReadTimeout = GSNetworkCommon.defaultTimeout;
                    StreamReader reader = new StreamReader(netStream);
                    string xmlstring = "";
                    string endCondtionChecker = "";
                    while (endCondtionChecker != GSNetworkCommon.packetEndCondition)
                    {
                        char oneChar = (char)reader.Read();
                        xmlstring += oneChar;
                        if (endCondtionChecker.Length >= GSNetworkCommon.packetEndCondition.Length)
                        {
                            endCondtionChecker = endCondtionChecker.Remove(0, 1);
                        }
                        endCondtionChecker += oneChar;
                    }
                    netStream.Flush();
                    Console.WriteLine(xmlstring);
                    StringReader sreader = new StringReader(xmlstring);
                    recievedRaw = (rawPacket)serializer.Deserialize(sreader);
                } 
                catch (SocketException e)
                {
                    // connection error
                    // Console.WriteLine(e.ToString());
                    recievedRaw.packetType = GSPacketType.T_ERR;
                }
                catch (IOException e)
                {
                    recievedRaw.packetType = GSPacketType.T_TIMEOUT;
                }
                catch (Exception e)
                {
                    // unexpected exception
                    GSNetworkCommon.Panic(e);
                    recievedRaw.packetType = GSPacketType.T_ERR;
                }
				// call delegate
                switch (recievedRaw.packetType)
                {
                    case GSPacketType.T_BATTLE:
                        //battle packet
                        if (SubmitMovesCallback != null) SubmitMovesCallback(new GSPacket(recievedRaw));
                        break;
                    case GSPacketType.T_LOGIN:
                        // login packet
                        if (LoginCallback != null) LoginCallback(new GSLoginPacket(recievedRaw));
                        break;
                    case GSPacketType.T_MATCHMAKING:
                        // match making
                        if (MatchMakingCallback != null) MatchMakingCallback(new GSMatchMakingPacket(recievedRaw));
                        break;
                    case GSPacketType.T_TIMEOUT:
                        // timeout
                        if (TimeoutCallback != null) TimeoutCallback(new GSPacket(recievedRaw));
                        break;
                    case GSPacketType.T_ERR:
                        // error
                        if (ErrorCallback != null) ErrorCallback(new GSPacket(recievedRaw));
                        break;
                    case GSPacketType.T_NULL:
                        // error
                        if (ErrorCallback != null) ErrorCallback(new GSPacket(recievedRaw));
                        break;
                    default:
                        // custom
                        if (GenericSendCallback != null) GenericSendCallback(new GSPacket(recievedRaw));
                        break;
                }
			}
            netStream.Close();
            clientConnection.Close();
		}

		public void Start()
		{
			// run network_handler thread;
            try
            {
                threadEnd = false;
                // create thread
                _thread = new Thread(new ThreadStart(this.threadMain));
                // start the handler
                _thread.Start();

            }
            catch (Exception e)
            {
                GSNetworkCommon.Panic(e);
            }
		}

		public void Terminate()
		{
			threadEnd = true;
		}

        #endregion
        
	}
}

