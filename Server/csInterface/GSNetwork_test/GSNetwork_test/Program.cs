﻿using System;
using GSNetwork;

namespace GSNetwork_test
{
    public class callbacks
    {
        public void LoginCallback(GSLoginPacket rpacket) {
            Console.WriteLine(rpacket.packetType.ToString());
            Console.WriteLine(rpacket.uid);
        }
        public void MatchMakingCallback(GSMatchMakingPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
        public void SubmitMovesCallback(GSPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
        public void BaseRequestCallback(GSPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
        public void GenericSendCallback(GSPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
        public void ErrorCallback(GSPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
        public void TimeoutCallback(GSPacket rpacket) { Console.WriteLine(rpacket.packetType.ToString()); }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Application starts.");

            network_handler network_interface = network_handler.Instance;

            Console.WriteLine("network_interface, instance");

            callbacks cbs = new callbacks();

            network_interface.registerLoginCallback(new network_handler.LoginCallbackDelegate(cbs.LoginCallback));
            network_interface.registerMatchMakingCallback(new network_handler.MatchMakingCallbackDelegate(cbs.MatchMakingCallback));
            network_interface.registerSubmitMovesCallback(new network_handler.recieveDelegate(cbs.SubmitMovesCallback));
            network_interface.registerBaseRequestCallback(new network_handler.recieveDelegate(cbs.BaseRequestCallback));
            network_interface.registerGenericSendCallback(new network_handler.recieveDelegate(cbs.GenericSendCallback));
            network_interface.registerErrorCallback(new network_handler.recieveDelegate(cbs.ErrorCallback));
            network_interface.registerTimeoutCallback(new network_handler.recieveDelegate(cbs.TimeoutCallback));

            Console.WriteLine("network_interface, register callbacks");

            network_interface.Start();

            Console.WriteLine("network_interface, start");

            network_interface.login("50");
            Console.WriteLine("network_interface, login");

            Console.ReadLine();

            network_interface.logout();
        }
    }
}
